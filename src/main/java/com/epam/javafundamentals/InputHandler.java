package com.epam.javafundamentals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.GregorianCalendar;

public class InputHandler {
    private InputValidator inputValidator = new InputValidator();

    public void processInput() {                                                                 //method to process input data
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));       //using buffered reader to read user's input
        System.out.println("Please enter a year (integer) and a month (integer from 1 to 12). Separate them using space...");
        boolean repeatInputRequest = true;
        while (repeatInputRequest) {                                                             //implementing continuous app execution
            try {
                String inputData = inputReader.readLine();                                        //reading input
                String[] arrayWithYearAndMonth = inputData.split(" ");                      //splitting input line to separate values
                if (arrayWithYearAndMonth.length > 2) {                                           //checking if user has entered 2 values
                    System.out.println("You made more than 2 entries! Two is needed (year and month). Please, try again...");
                    continue;
                } else if (arrayWithYearAndMonth.length < 2) {
                    System.out.println("You made less than 2 entries! Two is needed (year and month). Please, try again...");
                    continue;
                } else {
                    ValidationResult validationResult = inputValidator.validateInput(arrayWithYearAndMonth);
                    if (validationResult.isValidInput) {                                          //validating input
                        repeatInputRequest = false;
                        printYearAndMonth(validationResult.parsedData);                           //printing entered year and month
                    } else {
                        System.out.println("Year or month are negative, or month is not in correct range (1-12)! Please, try again...");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
        try {
            inputReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printYearAndMonth(int[] yearAndMonthArray) {                                 //method to print year and month
        for (int i = 0; i < yearAndMonthArray.length; i++) {
            switch (i) {
                case (0):                                                                      //printing out year
                    System.out.print(yearAndMonthArray[i]);
                    break;
                case (1):                                                                      //printing out month
                    switch (yearAndMonthArray[i]) {
                        case 1:
                            System.out.print(" January");
                            break;
                        case 2:
                            System.out.print(" February");
                            break;
                        case 3:
                            System.out.print(" March");
                            break;
                        case 4:
                            System.out.print(" April");
                            break;
                        case 5:
                            System.out.print(" May");
                            break;
                        case 6:
                            System.out.print(" June");
                            break;
                        case 7:
                            System.out.print(" July");
                            break;
                        case 8:
                            System.out.print(" August");
                            break;
                        case 9:
                            System.out.print(" September");
                            break;
                        case 10:
                            System.out.print(" October");
                            break;
                        case 11:
                            System.out.print(" November");
                            break;
                        case 12:
                            System.out.print(" December");
                            break;
                        default:
                            System.out.println(" Month");
                            break;
                    }
                    break;
                default:
                    System.out.print("Year");
                    break;
            }
        }
        GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();  //instantiating gregorian calendar class
        if (gregorianCalendar.isLeapYear(yearAndMonthArray[1])) {                                   //checking if entered year is leap year
            System.out.print(" - leap year");
        } else {
            System.out.print(" - not leap year");
        }
    }
}
