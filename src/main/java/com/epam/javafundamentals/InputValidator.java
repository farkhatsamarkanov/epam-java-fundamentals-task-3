package com.epam.javafundamentals;

public class InputValidator {
    public ValidationResult validateInput(String[] arrayWithYearAndMonth) {                   //method to validate input data. If data is valid then parse it
        ValidationResult validationResult = new ValidationResult();
        validationResult.parsedData = new int[arrayWithYearAndMonth.length];
        validationResult.isValidInput = true;
        for (int i = 0; i < arrayWithYearAndMonth.length; i++) {                               //looping through input data
            try {
                validationResult.parsedData[i] = Integer.parseInt(arrayWithYearAndMonth[i]);   //checking if entered data are integers
                if (Integer.parseInt(arrayWithYearAndMonth[i]) < 0) {                          //checking if entered data are negative integers
                    validationResult.isValidInput = false;
                } else if (i > 0 && Integer.parseInt(arrayWithYearAndMonth[i]) > 12 || Integer.parseInt(arrayWithYearAndMonth[i]) < 1) {
                    validationResult.isValidInput = false;                                     //checking if entered month is between 1 and 12
                }
            } catch (NumberFormatException e) {                                                //if one of entered values is not in integer then print warning
                System.out.println("Either year or a month is not an integer!");
                validationResult.isValidInput = false;
                break;
            }
        }
        return validationResult;
    }
}
