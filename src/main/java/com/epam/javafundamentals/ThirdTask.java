package com.epam.javafundamentals;


public class ThirdTask
{
    public static void main( String[] args )
    {
        InputHandler inputHandler = new InputHandler();         //instantiating input handling class
        inputHandler.processInput();                            //process user's input
    }
}
